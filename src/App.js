import './App.css';
import {useState} from "react";
import Ingredient from './Components/Ingredient';
import Hamburger from "./Components/Hamburger";
import meatImage from './assets/burger.png';
import cheeseImage from './assets/cheese.png';
import saladImage from './assets/salad.png';
import beconImage from './assets/bacon-strips.png';


const App = () => {
    const INGREDIENTS = [
        {name: 'Meat', price: '50', image: meatImage},
        {name: 'Cheese', price: '20', image: cheeseImage},
        {name: 'Salad', price: '5', image: saladImage},
        {name: 'Bacon', price: '30', image: beconImage},
    ];

    const prices = {
       'Meat': 50,
       'Cheese': 20,
       'Salad': 5,
        'Bacon' : 30,
    };

    const [ingredients, setIngredients] = useState([
        {name: 'Meat', count: 0, id: 1},
        {name: 'Cheese', count: 0, id: 2},
        {name: 'Salad', count: 0, id: 3},
        {name: 'Bacon', count: 0, id: 4}

    ]);

    const getTotalCount = ingredients.reduce((sum, ingredients) => {
       return sum + prices[ingredients.name] * ingredients.count;
   }, 20);

    const increaseCount = id => {
        setIngredients(ingredients.map(ing => {
            if(ing.id === id) {
                return {...ing, count: ing.count + 1}
            }

            return ing;
        }))
    };

    const removeIngredient = id => {
        setIngredients(ingredients.map(ing => {
            if(ing.id === id) {
                return {...ing, count: ing.count - 1}
            }

            return ing;
        }))
    };

    const createIngredient = ingredients.map((el, i) => (
        <Ingredient
            key={el.id}
            id={el.id}
            image={INGREDIENTS[i].image}
            ingredientName={el.name}
            count={el.count}
            onRemove={() => removeIngredient(el.id)}
            onHeaderClick={() => increaseCount(el.id)}
        />
    ));


    return (
        <div className="Container">

            <div className="IngredientBlock">
                <h2>Ingredients</h2>
                {createIngredient}
            </div>
            <Hamburger ingredients={ingredients}/>
            <p className="price">Total: {getTotalCount} </p>
        </div>

    )

};

export default App;
