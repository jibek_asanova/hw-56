import React from 'react';
import './Hamburger.css';

const Hamburger = ({ingredients}) => {


    let burgerIngredients = [];
    for (let i = 0; i < ingredients.length; i++) {
        for(let j = 0; j < ingredients[i].count; j++) {
            burgerIngredients.push(<div className={ingredients[i].name}/>);
        }
    }

    return (
        <div className="Hamburger">
            <h2>Hamburger</h2>
            <div className="BreadTop">
                <div className="Seeds1"></div>
                <div className="Seeds2"></div>
            </div>
            {burgerIngredients}
            <div className="BreadBottom"></div>
        </div>
    );
};

export default Hamburger;