import React from 'react';
import './Ingredient.css';
import deleteIcon from '../assets/delete.png';

const Ingredient = ({ingredientName, count, onRemove, onHeaderClick, image}) => {
    return (
        <div className="Ingredient">
            <img onClick={onHeaderClick} src={image} alt='deleteIcon' width='50px' className="click"/>
            <p>{ingredientName}</p>
            <p>x{count}</p>
            <button onClick={onRemove} disabled={count === 0} className="btn"><img src={deleteIcon} alt='deleteIcon' width='30px' className="click"/></button>
        </div>
    );
};

export default Ingredient;